import {
  RiAddFill,
  RiCloseFill,
  RiMenu3Fill,
  RiPieChart2Fill,
  RiUserLine,
} from "react-icons/ri";
import Sidebar from "./components/Sidebar";
import { useState } from "react";
import Content from "./components/Content";
import Main from "./components/Main";
import Services from "./components/Services";
import Plans from "./components/Plans";
import Recomendation from "./components/Recomendation";

function App() {
  // Components
  const [showMenu, setShowMenu] = useState(false);
  const [showOrder, setShowOrder] = useState(false);

  const toggleMenu = () => {
    setShowMenu(!showMenu);
    setShowOrder(false);
  };

  const toggleOrder = () => {
    setShowOrder(!showOrder);
    setShowMenu(false);
  };

  const [isDarkMode, setIsDarkMode] = useState(false); // Estado para rastrear el modo oscuro o claro

  // eslint-disable-next-line no-unused-vars
  const toggleDarkMode = () => {
    setIsDarkMode(!isDarkMode);
  };

  return (
    <>
      <div className="bg-[#E4E6EB] w-full min-h-screen">
        <Sidebar
          classCustom={
            isDarkMode
              ? "bg-[#191919] text-[#E4E6EB]"
              : "bg-[#fff] text-gray-900"
          }
          showMenu={showMenu}
        />
        <Main
          showOrder={showOrder}
          setShowOrder={setShowOrder}
          classCustom={
            isDarkMode
              ? "bg-[#191919] text-[#E4E6EB]"
              : "bg-[#FAFAFA] text-[#333]"
          }
        />
        <Content
          classCustom={
            isDarkMode ? "bg-[#191919] text-[#B5B0C7]" : "bg-[#fff] text-[#333]"
          }
        />
        {/* <button
          onClick={toggleDarkMode}
          className="fixed right-72 w-32 rounded-full bg-white text-black shadow-md"
        >
          {isDarkMode ? "Dark" : "Light"}
        </button> */}

        {/* My services */}
        <Services
          classCustom={
            isDarkMode ? "bg-[#191919] text-[#E4E6EB]" : "text-gray-900"
          }
        />

        {/* Price plans */}
        <Plans
          classCustom={
            isDarkMode ? "bg-[#191919] text-[#B5B0C7]" : "text-[#333]"
          }
        />

        {/* Recomendation */}
        <Recomendation
          classCustom={
            isDarkMode ? "bg-[#191919] text-[#B5B0C7]" : "text-[#333]"
          }
        />

        {/* Menú móvil */}
        <nav className="bg-[#fff] lg:hidden fixed w-full bottom-0 left-0 text-3xl  py-2 px-8 flex items-center justify-between text-[#000]">
          <button className="p-2">
            <RiUserLine />
          </button>
          <button className="p-2">
            <RiAddFill />
          </button>
          <button onClick={toggleOrder} className="p-2">
            <RiPieChart2Fill />
          </button>
          <button onClick={toggleMenu} className=" p-2">
            {showMenu ? <RiCloseFill /> : <RiMenu3Fill />}
          </button>
        </nav>
      </div>
    </>
  );
}

export default App;
