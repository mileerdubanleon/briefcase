import {
  RiBriefcase4Fill,
  RiChat4Fill,
  RiCloseFill,
  RiFileCodeFill,
  RiGitRepositoryFill,
  RiHome2Fill,
  RiMoonFill,
  RiQuillPenFill,
} from "react-icons/ri";

const Main = (props) => {
  // eslint-disable-next-line react/prop-types
  const { showOrder, setShowOrder, classCustom } = props;
  return (
    <div
      className={`${classCustom} lg:col-span-2 fixed top-0 w-28 lg:w-28 lg:right-0 h-full shadow transition-all z-50 ${
        showOrder ? "right-0" : "-right-full"
      }`}
    >
      {/* dark/light */}
      <div className="flex justify-center my-8">
        <RiMoonFill className="text-4xl text-gray-800" />
      </div>

      <div className="flex flex-col items-center gap-10 my-32 ">
        <a className="bg-[#FFB400] p-3 rounded-full" href="#">
          <RiHome2Fill className="text-[#191919]" />
        </a>
        <div className="group flex">
          <a
            className="bg-[#F0F0F6] group-hover:bg-[#FFB400] p-3 rounded-full"
            href="#"
          >
            <RiFileCodeFill className="text-gray-600 group-hover:text-[#000]" />
          </a>
        </div>
        <div className="group flex">
          <a
            className="bg-[#F0F0F6] group-hover:bg-[#FFB400] p-3 rounded-full"
            href="#"
          >
            <RiGitRepositoryFill className="text-gray-600 group-hover:text-[#000]" />
          </a>
        </div>
        <div className="group flex">
          <a
            className="bg-[#F0F0F6] group-hover:bg-[#FFB400] p-3 rounded-full"
            href="#"
          >
            <RiBriefcase4Fill className="text-gray-600 group-hover:text-[#000]" />
          </a>
        </div>
        <div className="group flex">
          <a
            className="bg-[#F0F0F6] group-hover:bg-[#FFB400] p-3 rounded-full"
            href="#"
          >
            <RiQuillPenFill className="text-gray-600 group-hover:text-[#000]" />
          </a>
        </div>
        <div className="group flex">
          <a
            className="bg-[#F0F0F6] group-hover:bg-[#FFB400] p-3 rounded-full"
            href="#"
          >
            <RiChat4Fill className="text-gray-600 group-hover:text-[#000]" />
          </a>
        </div>
      </div>

      {/* btn close mobile*/}
      <div className="relative pt-16 lg:pt-8 text-gray-300 p-4 h-full">
        <RiCloseFill
          onClick={() => setShowOrder(false)}
          className="lg:hidden absolute left-4 top-4 p-3 box-content text-gray-800 rounded-full text-xl"
        />
      </div>
    </div>
  );
};

export default Main;
