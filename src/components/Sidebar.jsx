import PropTypes from "prop-types";
import {
  RiFacebookFill,
  RiInstagramFill,
  RiLinkedinFill,
  RiTelegramFill,
  RiTwitterFill,
  RiYoutubeFill,
} from "react-icons/ri";
const Sidebar = (props) => {
  const { showMenu, classCustom } = props;
  return (
    <div
      className={`${classCustom} shadow-md fixed lg:left-0 top-0 w-[320px] h-full overflow-y-auto transition-all ${
        showMenu ? "left-0" : "-left-full"
      }`}
    >
      {/* container */}
      <div className="p-4 ">
        {/* img profile */}
        <div className="flex justify-center items-center">
          <img
            src="../public/img/logo1.png"
            className="w-32 h-32 my-5 rounded-full"
          />
        </div>
        <h1 className="flex justify-center items-center  font-semibold">
          Mileer duban León
        </h1>
        <span className="flex justify-center items-center my-2 text-gray-600">
          Front-End Developer
        </span>

        {/* Social media */}
        <div className="flex justify-center items-center gap-4 py-2 pb-8 border-b-2">
          <div className="bg-[#FFB400] rounded-full p-1 text-gray-800">
            <RiFacebookFill className="" />
          </div>
          <div className="bg-[#FFB400] rounded-full p-1 text-gray-800">
            <RiInstagramFill />
          </div>
          <div className="bg-[#FFB400] rounded-full p-1 text-gray-800">
            <RiTwitterFill />
          </div>
          <div className="bg-[#FFB400] rounded-full p-1 text-gray-800">
            <RiLinkedinFill />
          </div>
          <div className="bg-[#FFB400] rounded-full p-1 text-gray-800">
            <RiYoutubeFill />
          </div>
          <div className="bg-[#FFB400] rounded-full p-1 text-gray-800">
            <RiTelegramFill />
          </div>
        </div>

        {/* datos */}
        <div className="border-b-2">
          <div className="flex justify-between px-5 mt-6">
            <p className="bg-[#FFB400] w-auto px-3 rounded text-gray-700">
              Age:
            </p>
            <span>26</span>
          </div>
          <div className="flex justify-between px-5 my-3">
            <p className="bg-[#FFB400] w-auto px-3 rounded text-gray-700">
              Residence:
            </p>
            <span>Bogotá</span>
          </div>
          <div className="flex justify-between px-5 my-3">
            <p className="bg-[#FFB400] w-auto px-3 rounded text-gray-700">
              Freelance:
            </p>
            <span className="text-[#7EB942]">Available</span>
          </div>
          <div className="flex justify-between px-5 my-3 mb-7">
            <p className="bg-[#FFB400] w-auto px-3 rounded text-gray-700">
              Address:
            </p>
            <span>Calle 8 #4-24</span>
          </div>
        </div>

        {/* Languages */}
        <div className="border-b-2 py-6">
          <h1 className="px-5 font-semi-bold text-2xl">Lenguajes</h1>
          <div>
            <div className="flex justify-between px-5 mt-5">
              <p className="w-auto ">English</p>
              <span>20%</span>
            </div>
            <div className="px-5 ">
              <div className="w-full h-3 bg-gray-300 mt-2 p-1 rounded-md">
                <div className="h-full bg-[#FFB400]" style={{ width: "20%" }} />
              </div>
            </div>
          </div>
          <div>
            <div className="flex justify-between px-5 mt-3">
              <p className="w-auto ">Spanish</p>
              <span>100%</span>
            </div>
            <div className="px-5 ">
              <div className="w-full h-3 bg-gray-300 mt-2 p-1 rounded-md">
                <div
                  className="h-full bg-[#FFB400]"
                  style={{ width: "100%" }}
                />
              </div>
            </div>
          </div>
          <div>
            <div className="flex justify-between px-5 mt-3">
              <p className="w-auto ">Portugues</p>
              <span>50%</span>
            </div>
            <div className="px-5 ">
              <div className="w-full h-3 bg-gray-300 mt-2 p-1 rounded-md">
                <div className="h-full bg-[#FFB400]" style={{ width: "50%" }} />
              </div>
            </div>
          </div>
        </div>

        {/* Skills */}
        <div className="border-b-2 py-6">
          <h1 className="px-5 font-semi-bold my-2 text-2xl">Skills</h1>
          <div>
            <div className="flex justify-between px-5 mt-4">
              <p className="w-auto ">Html</p>
              <span>90%</span>
            </div>
            <div className="px-5 ">
              <div className="w-full h-3 bg-gray-300 mt-2 p-1 rounded-md">
                <div className="h-full bg-[#FFB400]" style={{ width: "90%" }} />
              </div>
            </div>
          </div>
          <div>
            <div className="flex justify-between px-5 mt-3">
              <p className="w-auto ">CSS</p>
              <span>100%</span>
            </div>
            <div className="px-5 ">
              <div className="w-full h-3 bg-gray-300 mt-2 p-1 rounded-md">
                <div className="h-full bg-[#FFB400]" style={{ width: "85%" }} />
              </div>
            </div>
          </div>
          <div>
            <div className="flex justify-between px-5 mt-3">
              <p className="w-auto ">Js</p>
              <span>80%</span>
            </div>
            <div className="px-5 ">
              <div className="w-full h-3 bg-gray-300 mt-2 p-1 rounded-md">
                <div className="h-full bg-[#FFB400]" style={{ width: "80%" }} />
              </div>
            </div>
          </div>
          <div>
            <div className="flex justify-between px-5 mt-3">
              <p className="w-auto ">PHP</p>
              <span>75%</span>
            </div>
            <div className="px-5 ">
              <div className="w-full h-3 bg-gray-300 mt-2 p-1 rounded-md">
                <div className="h-full bg-[#FFB400]" style={{ width: "75%" }} />
              </div>
            </div>
          </div>
          <div>
            <div className="flex justify-between px-5 mt-3">
              <p className="w-auto ">Wordpress</p>
              <span>85%</span>
            </div>
            <div className="px-5 ">
              <div className="w-full h-3 bg-gray-300 mt-2 p-1 rounded-md">
                <div className="h-full bg-[#FFB400]" style={{ width: "85%" }} />
              </div>
            </div>
          </div>
        </div>

        {/* Extra Skill */}
        <div className="border-b-2 py-6">
          <h1 className="px-5 font-semi-bold my-2 text-2xl">Extra Skills</h1>
          <div className="flex justify-between px-6 mt-4 gap-5">
            <svg
              className=""
              width="15"
              height="15"
              viewBox="0 0 15 15"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <rect x="0.5" y="0.5" width="9" height="9" stroke="#FFB400" />
              <rect x="5.5" y="5.5" width="9" height="9" stroke="#FFB400" />
              <rect x="0.5" y="0.5" width="9" height="9" stroke="#FFB400" />
              <rect x="5.5" y="5.5" width="9" height="9" stroke="#FFB400" />
            </svg>
            <span className="justify-start w-full">Bootstrap, Materialize</span>
          </div>
          <div className="flex justify-between px-6 mt-2 gap-5">
            <svg
              className=""
              width="15"
              height="15"
              viewBox="0 0 15 15"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <rect x="0.5" y="0.5" width="9" height="9" stroke="#FFB400" />
              <rect x="5.5" y="5.5" width="9" height="9" stroke="#FFB400" />
              <rect x="0.5" y="0.5" width="9" height="9" stroke="#FFB400" />
              <rect x="5.5" y="5.5" width="9" height="9" stroke="#FFB400" />
            </svg>
            <span className="justify-start w-full">Stylus, Tailwind, Less</span>
          </div>
          <div className="flex justify-between px-6 mt-2 gap-5">
            <svg
              className=""
              width="15"
              height="15"
              viewBox="0 0 15 15"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <rect x="0.5" y="0.5" width="9" height="9" stroke="#FFB400" />
              <rect x="5.5" y="5.5" width="9" height="9" stroke="#FFB400" />
              <rect x="0.5" y="0.5" width="9" height="9" stroke="#FFB400" />
              <rect x="5.5" y="5.5" width="9" height="9" stroke="#FFB400" />
            </svg>
            <span className="justify-start w-full">Gulp, Figma, Grunt</span>
          </div>
          <div className="flex justify-between px-6 mt-2 gap-5">
            <svg
              className=""
              width="15"
              height="15"
              viewBox="0 0 15 15"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <rect x="0.5" y="0.5" width="9" height="9" stroke="#FFB400" />
              <rect x="5.5" y="5.5" width="9" height="9" stroke="#FFB400" />
              <rect x="0.5" y="0.5" width="9" height="9" stroke="#FFB400" />
              <rect x="5.5" y="5.5" width="9" height="9" stroke="#FFB400" />
            </svg>
            <span className="justify-start w-full">Git</span>
          </div>
        </div>

        {/* Button dowload */}
        <div className="my-6 flex mb-20">
          <button className="bg-[#FFB400] uppercase h-10 font-semibold w-full mx-6 flex justify-center items-center gap-6 rounded">
            Download cv
            <svg
              width="12"
              height="13"
              viewBox="0 0 12 13"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M1.33317 10.9523H10.6665V6.28564H11.8332V11.619C11.8332 11.7958 11.7717 11.9654 11.6623 12.0904C11.5529 12.2154 11.4045 12.2856 11.2498 12.2856H0.749837C0.595128 12.2856 0.446754 12.2154 0.337358 12.0904C0.227962 11.9654 0.166504 11.7958 0.166504 11.619V6.28564H1.33317V10.9523ZM7.1665 4.28564H10.0832L5.99984 8.95231L1.9165 4.28564H4.83317V0.285645H7.1665V4.28564Z"
                fill="#2B2B2B"
              />
            </svg>
          </button>
        </div>
      </div>
    </div>
  );
};

Sidebar.propTypes = {
  showMenu: PropTypes.bool.isRequired,
  classCustom: PropTypes.any.isRequired,
};

export default Sidebar;
