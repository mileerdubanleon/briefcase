import { RiCheckFill, RiCloseFill } from "react-icons/ri";
import PropTypes from "prop-types";

const Plans = (props) => {
  const { classCustom } = props;
  return (
    <div className="lg:pl-72 lg:pr-[210px] h-full flex flex-col items-center justify-center ">
      <div
        className={`${classCustom} md:p-16 lg:p-6 w-full h-full lg:ml-32 mb-8`}
      >
        <h1 className="w-full h-12  flex justify-center font-semi-bold text-3xl">
          Price Plans
        </h1>
        <p className="my-3 mb-12 lg:px-72 px-8 text-center text-gray-500">
          Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet
          sint. Velit officia consequat duis enim velit mollit. lorem ipsum
        </p>
        <div className="lg:flex lg:mx-0 mx-10 gap-8 mt-6">
          {/* card */}
          <div className="bg-[#fff] w-[310px] h-full flex flex-col items-center gap-1">
            <h1 className="text-2xl font-semi-bold mt-10 my-3">Silver</h1>
            <h1 className="font-bold text-3xl">
              $0.00<span className="font-thin text-lg"> /Hour</span>
            </h1>

            <p className="text-gray-500 flex justify-center items-center px-8 mb-6 text-center">
              For Most Businesses That Want To Optimize Web Queries
            </p>

            <ul className="w-full px-10 mb-8 text-gray-600">
              <li className="flex gap-4">
                <RiCheckFill className="text-2xl text-[#FFB400]" />
                <span>UI Design</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCheckFill className="text-2xl text-[#FFB400]" />
                <span>Web Development</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCloseFill className="text-2xl text-gray-600" />
                <span>Logo Design</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCloseFill className="text-2xl text-gray-600" />
                <span>Seo optimization</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCloseFill className="text-2xl text-gray-600" />
                <span>WordPress Integration</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCloseFill className="text-2xl text-gray-600" />
                <span>5 Websites</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCloseFill className="text-2xl text-gray-600" />
                <span>Unlimited User</span>
              </li>
              <li className="flex gap-4">
                <RiCloseFill className="text-2xl text-gray-600" />
                <span>20 GB Bandiwith</span>
              </li>
            </ul>
            <button className="mb-10 bg-[#f3f3f3] px-10 py-2 rounded-full shadow-lg font-bold uppercase text-sm">
              Order now
            </button>
          </div>
          <div className="bg-[#fff] w-[310px] h-full flex flex-col items-center gap-1 my-8 lg:my-0 shadow-2xl">
            <span className="bg-[#FFB400] w-full flex justify-center py-1">
              Most Popular
            </span>
            <h1 className="text-2xl font-semi-bold mt-2 my-3">Gold</h1>
            <h1 className="font-bold text-3xl">
              $50.00<span className="font-thin text-lg"> /Hour</span>
            </h1>

            <p className="text-gray-500 flex justify-center items-center px-8 mb-6 text-center">
              For Most Businesses That Want To Optimize Web Queries
            </p>

            <ul className="w-full px-10 mb-8 text-gray-600">
              <li className="flex gap-4">
                <RiCheckFill className="text-2xl text-[#FFB400]" />
                <span>UI Design</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCheckFill className="text-2xl text-[#FFB400]" />
                <span>Web Development</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCheckFill className="text-2xl text-[#FFB400]" />
                <span>Logo Design</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCheckFill className="text-2xl text-[#FFB400]" />
                <span>Seo optimization</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCloseFill className="text-2xl text-gray-600" />
                <span>WordPress Integration</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCloseFill className="text-2xl text-gray-600" />
                <span>5 Websites</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCloseFill className="text-2xl text-gray-600" />
                <span>Unlimited User</span>
              </li>
              <li className="flex gap-4">
                <RiCloseFill className="text-2xl text-gray-600" />
                <span>20 GB Bandiwith</span>
              </li>
            </ul>
            <button className="mb-10 bg-[#FFB400] px-10 py-2 rounded-full shadow-lg font-bold uppercase text-sm">
              Order now
            </button>
          </div>
          <div className="bg-[#fff] w-[310px] h-full flex flex-col items-center gap-1">
            <h1 className="text-2xl font-semi-bold mt-10 my-3">Dimond</h1>
            <h1 className="font-bold text-3xl">
              $80.00<span className="font-thin text-lg"> /Hour</span>
            </h1>

            <p className="text-gray-500 flex justify-center items-center px-8 mb-6 text-center">
              For Most Businesses That Want To Optimize Web Queries
            </p>

            <ul className="w-full px-10 mb-8 text-gray-600">
              <li className="flex gap-4">
                <RiCheckFill className="text-2xl text-[#FFB400]" />
                <span>UI Design</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCheckFill className="text-2xl text-[#FFB400]" />
                <span>Web Development</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCheckFill className="text-2xl text-[#FFB400]" />
                <span>Logo Design</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCheckFill className="text-2xl text-[#FFB400]" />
                <span>Seo optimization</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCheckFill className="text-2xl text-[#FFB400]" />
                <span>WordPress Integration</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCheckFill className="text-2xl text-[#FFB400]" />
                <span>5 Websites</span>
              </li>
              <li className="flex gap-4 my-2">
                <RiCheckFill className="text-2xl text-[#FFB400]" />
                <span>Unlimited User</span>
              </li>
              <li className="flex gap-4">
                <RiCheckFill className="text-2xl text-[#FFB400]" />
                <span>20 GB Bandiwith</span>
              </li>
            </ul>
            <button className="mb-10 bg-[#f3f3f3] px-10 py-2 rounded-full shadow-lg font-bold uppercase text-sm">
              Order now
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

Plans.propTypes = {
  classCustom: PropTypes.any.isRequired,
};

export default Plans;
