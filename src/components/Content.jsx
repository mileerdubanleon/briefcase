import PropTypes from "prop-types";
const Content = (props) => {
  const { classCustom } = props;
  return (
    <main className="lg:pl-72 lg:pr-[210px] h-[400px] flex flex-col items-center justify-center ">
      <div
        className={`${classCustom} md:p-16 lg:p-6 w-full h-full lg:ml-32 lg:flex justify-between shadow-md`}
      >
        <div className="">
          {/* Title */}
          <h1 className="font-bold lg:text-4xl mt-8 mx-8 text-3xl">
            I’m Rayan Adlrdard Front-end Developer{" "}
          </h1>
          <p className="mx-8 mt-4">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et,
            volutpat feugiat placerat lobortis. Natoque rutrum semper sed
            suspendisse nunc lectus.
          </p>
          <button className="uppercase bg-[#FFB400] text-gray-800 px-7 py-2 rounded mx-8 mt-8">
            hire Me{" "}
          </button>
        </div>
        <div className="w-full lg:px-0 md:px-72">
          <img
            src="../public/img/user.png"
            className="h-28 lg:h-[378px] lg:mx-32 md:h-32 lg:px-0 mx-60 flex justify-center items-center"
            alt=""
          />
        </div>
      </div>
    </main>
  );
};

Content.propTypes = {
  classCustom: PropTypes.any.isRequired,
};

export default Content;
