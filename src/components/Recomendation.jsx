import PropTypes from "prop-types";
import { RiStarFill } from "react-icons/ri";
const Recomendation = (props) => {
  const { classCustom } = props;
  return (
    <div className="lg:pl-72 lg:pr-[210px] h-full flex flex-col items-center justify-center ">
      <div
        className={`${classCustom} md:p-16 lg:p-6 w-full h-full lg:ml-32 mb-8`}
      >
        <h1 className="w-full h-12 flex justify-center font-semi-bold text-3xl">
          Recommendations
        </h1>
        <p className="my-3 mb-12 lg:px-72 px-8 text-center text-gray-500">
          Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet
          sint. Velit officia consequat duis enim velit mollit. lorem ipsum
        </p>
        <div className="lg:flex gap-6">
          <div className="lg:flex lg:mx-0 mx-10 gap-8 mt-6">
            {/* card */}
            <div className="bg-[#fff] w-[310px] h-full p-5 gap-1">
              <div className="flex text-[#FFB400] gap-2">
                <RiStarFill />
                <RiStarFill />
                <RiStarFill />
                <RiStarFill />
                <RiStarFill />
              </div>
              <h1 className="text-2xl font-semi-bold my-3">Great Quality!</h1>
              <p className="text-gray-600">
                Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                consequatur delectus at voluptats veniam odit doloremque?
                Blanditiis?
              </p>
              <div className="flex my-4">
                <img src="../public/img/james.png" alt="" />
                <div className="my-4 mx-4">
                  <h1 className="w-full font-semibold">James Gouse</h1>
                  <span className="text-gray-500 flex">Graphic Designer</span>
                </div>
              </div>
            </div>
          </div>
          <div className="lg:flex lg:mx-0 mx-10 gap-8 mt-6">
            {/* card */}
            <div className="bg-[#fff] w-[310px] h-full p-5 gap-1">
              <div className="flex text-[#FFB400] gap-2">
                <RiStarFill />
                <RiStarFill />
                <RiStarFill />
                <RiStarFill />
                <RiStarFill />
              </div>
              <h1 className="text-2xl font-semi-bold my-3">Amazing Work!</h1>
              <p className="text-gray-600">
                Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                consequatur delectus at voluptats veniam odit doloremque?
                Blanditiis?
              </p>
              <div className="flex my-4">
                <img src="../public/img/tiana.png" alt="" />
                <div className="my-4 mx-4">
                  <h1 className="w-full font-semibold">Tiana Philips</h1>
                  <span className="text-gray-500 flex">Photographer</span>
                </div>
              </div>
            </div>
          </div>
          <div className="lg:flex lg:mx-0 mx-10 gap-8 mt-6">
            {/* card */}
            <div className="bg-[#fff] w-[310px] h-full p-5 gap-1">
              <div className="flex text-[#FFB400] gap-2">
                <RiStarFill />
                <RiStarFill />
                <RiStarFill />
                <RiStarFill />
                <RiStarFill />
              </div>
              <h1 className="text-2xl font-semi-bold my-3">Great Quality!</h1>
              <p className="text-gray-600">
                Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                consequatur delectus at voluptats veniam odit doloremque?
                Blanditiis?
              </p>
              <div className="flex my-4">
                <img src="../public/img/talan.png" alt="" />
                <div className="my-4 mx-4">
                  <h1 className="w-full font-semibold">Talan Westervelt</h1>
                  <span className="text-gray-500 flex">Business Man</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Recomendation.propTypes = {
  classCustom: PropTypes.any.isRequired,
};

export default Recomendation;
